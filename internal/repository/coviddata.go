package repository

import (
	"addzero/lineman/covid19/internal/entity"
)

type CovidDataRepository interface {
	GetRaw() (*entity.Response, error)
}
