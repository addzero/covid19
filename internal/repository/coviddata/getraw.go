package coviddata

import (
	"addzero/lineman/covid19/internal/entity"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const (
	getCovidDataURL = "https://static.wongnai.com/devinterview/covid-cases.json"
)

func (c covidDataRepository) GetRaw() (*entity.Response, error) {
	req, reqErr := http.NewRequest(http.MethodGet, getCovidDataURL, nil)
	if reqErr != nil {
		return nil, reqErr
	}

	resp, respErr := c.clientHttp.Do(req)
	if respErr != nil {
		return nil, respErr
	}

	if resp == nil {
		return nil, fmt.Errorf("http request response cannot be nil")
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("invalid http status response %d", resp.StatusCode)
	}

	respBodyByte, respBodyErr := ioutil.ReadAll(resp.Body)
	if respBodyErr != nil {
		return nil, respBodyErr
	}

	var dataResp entity.Response
	marshalErr := json.Unmarshal(respBodyByte, &dataResp)
	if marshalErr != nil {
		return nil, marshalErr
	}

	return &dataResp, nil
}
