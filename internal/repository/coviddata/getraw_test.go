package coviddata

import (
	"addzero/lineman/covid19/internal/entity"
	"addzero/lineman/covid19/internal/service/mocks"
	"addzero/lineman/covid19/shared/pointer"
	"addzero/lineman/covid19/test"
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"testing"

	"github.com/stretchr/testify/mock"
)

func Test_covidDataRepository_GetRaw(t *testing.T) {
	type mockers struct {
		httpClientDoResponse *http.Response
		httpClientDoError    error
	}
	tests := []struct {
		name    string
		mockers mockers
		want    *entity.Response
		wantErr bool
	}{
		{
			name: "should fails if http request return error",
			mockers: mockers{
				httpClientDoError: fmt.Errorf("get request error"),
			},
			wantErr: true,
		},
		{
			name: "should fails if http request response is nil",
			mockers: mockers{
				httpClientDoError: nil,
				httpClientDoResponse: nil,
			},
			wantErr: true,
		},
		{
			name: "should fails if http request response is with status code not 200 (400)",
			mockers: mockers{
				httpClientDoError: nil,
				httpClientDoResponse: &http.Response{
					StatusCode: http.StatusBadRequest,
				},
			},
			wantErr: true,
		},
		{
			name: "should success if http request response is not json",
			mockers: mockers{
				httpClientDoError: nil,
				httpClientDoResponse: &http.Response{
					StatusCode: http.StatusOK,
					Body:       ioutil.NopCloser(bytes.NewBufferString("plan text")),
				},
			},
			wantErr: true,
		},
		{
			name: "should success if http request response as json",
			mockers: mockers{
				httpClientDoError: nil,
				httpClientDoResponse: &http.Response{
					StatusCode: http.StatusOK,
					Body: ioutil.NopCloser(bytes.NewBufferString(test.MockGetCovidDataSuccessJson)),
				},
			},
			wantErr: false,
			want: &entity.Response{Data: []entity.Data{
				{
					ConfirmDate: pointer.ToStringPtr("2021-05-04"),
					No: nil,
					Age: pointer.ToIntPtr(51),
					Gender: pointer.ToStringPtr("หญิง"),
					GenderEn: pointer.ToStringPtr("Female"),
					Nation: nil,
					NationEn: pointer.ToStringPtr("China"),
					Province: pointer.ToStringPtr("Phrae"),
					ProvinceID: pointer.ToUintPtr(46),
					District: nil,
					ProvinceEn: pointer.ToStringPtr("Phrae"),
					StatQuarantine: pointer.ToIntPtr(5),
				},
			}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			httpClient := new(mocks.HttpClient)
			httpClient.On("Do", mock.Anything).Return(tt.mockers.httpClientDoResponse, tt.mockers.httpClientDoError)
			param := ParamCovidDataRepository{
				ClientHttp: httpClient,
			}
			repo := NewCovidDataRepository(param)
			got, err := repo.GetRaw()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetRaw() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetRaw() got = %v, want %v", got, tt.want)
			}
		})
	}
}
