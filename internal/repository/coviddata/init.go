package coviddata

import (
	"addzero/lineman/covid19/internal/repository"
	"addzero/lineman/covid19/internal/service"
)

type covidDataRepository struct {
	clientHttp service.HttpClient
}

type ParamCovidDataRepository struct {
	ClientHttp service.HttpClient
}

func NewCovidDataRepository(param ParamCovidDataRepository) repository.CovidDataRepository {
	return &covidDataRepository{
		clientHttp: param.ClientHttp,
	}
}
