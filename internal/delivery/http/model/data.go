package model

type Data struct {
	ConfirmDate    *string `json:"ConfirmDate"`
	No             *uint   `json:"No,omitempty"`
	Age            *int    `json:"Age"`
	Gender         *string `json:"Gender"`
	GenderEn       *string `json:"GenderEn"`
	Nation         *string `json:"Nation,omitempty"`
	NationEn       *string `json:"NationEn"`
	Province       *string `json:"Province"`
	ProvinceID     *uint   `json:"ProvinceId"`
	District       *string `json:"District,omitempty"`
	ProvinceEn     *string `json:"ProvinceEn"`
	StatQuarantine *int    `json:"StatQuarantine"`
}

type Response struct {
	Data []Data `json:"Data"`
}
