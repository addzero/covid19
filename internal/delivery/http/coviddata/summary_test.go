package coviddata

import (
	"addzero/lineman/covid19/internal/entity"
	"addzero/lineman/covid19/internal/usecase/mocks"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func Test_covidDataHandler_Summary(t *testing.T) {
	type mockers struct {
		covidDataUseCaseGetSummaryResult *entity.SummarizeData
		covidDataUseCaseGetSummaryError error
	}
	tests := []struct {
		name   string
		mockers mockers
		wantStatusCode int
		wantBody string
	}{
		{
			name: "should fails with http status 500 if covid data usecase get summary error",
			mockers: mockers{
				covidDataUseCaseGetSummaryResult: nil,
				covidDataUseCaseGetSummaryError: fmt.Errorf("covid data usecase get summary error"),
			},
			wantStatusCode: http.StatusInternalServerError,
		},
		{
			name: "success",
			mockers: mockers{
				covidDataUseCaseGetSummaryResult: &entity.SummarizeData{
					Province: map[string]uint{"Bangkok": 1},
					AgeGroup: map[entity.AgeGroupKey]uint{"61+": 1},
				},
				covidDataUseCaseGetSummaryError: nil,
			},
			wantStatusCode: http.StatusOK,
			wantBody: `{"Province":{"Bangkok":1},"AgeGroup":{"61+":1}}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			covidDataUC := new(mocks.CovidDataUseCase)
			covidDataUC.On("GetSummary").Return(tt.mockers.covidDataUseCaseGetSummaryResult, tt.mockers.covidDataUseCaseGetSummaryError)
			ginEng := gin.Default()
			param := ParamCovidDataHandler{
				CovidDataUseCase: covidDataUC,
				GinEngine: ginEng,
			}

			NewCovidDataHandler(param)

 			reqUrl := "/covid/summary"
 			req, _ := http.NewRequest("GET", reqUrl, nil)
 			res := httptest.NewRecorder()
 			ginEng.ServeHTTP(res, req)
 			if tt.wantStatusCode != res.Code {
 				t.Errorf("http covid/summary request got status = %v, want status = %v", res.Code, tt.wantStatusCode)
			}

			if !reflect.DeepEqual(tt.wantBody, res.Body.String()) {
				t.Errorf("http covid/summary request got body = %v, want body = %v", res.Body.String(), tt.wantBody)
			}
		})
	}
}
