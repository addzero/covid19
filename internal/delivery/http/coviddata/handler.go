package coviddata

import (
	"addzero/lineman/covid19/internal/usecase"
	"github.com/gin-gonic/gin"
)

type ParamCovidDataHandler struct {
	GinEngine *gin.Engine
	CovidDataUseCase usecase.CovidDataUseCase
}

type covidDataHandler struct {
	covidDataUseCase usecase.CovidDataUseCase
}

func NewCovidDataHandler(param ParamCovidDataHandler) {
	handler := covidDataHandler{covidDataUseCase: param.CovidDataUseCase}

	v1 := param.GinEngine.Group("covid")
	v1.GET("/summary", handler.Summary)
}
