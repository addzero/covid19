package coviddata

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func (h *covidDataHandler) Summary(ctx *gin.Context) {
	sum, sumErr := h.covidDataUseCase.GetSummary()
	if sumErr != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	ctx.JSON(http.StatusOK, sum)
	return
}
