package entity

type Data struct {
	ConfirmDate    *string
	No             *uint
	Age            *int
	Gender         *string
	GenderEn       *string
	Nation         *string
	NationEn       *string
	Province       *string
	ProvinceID     *uint
	District       *string
	ProvinceEn     *string
	StatQuarantine *int
}

type Response struct {
	Data []Data
}

type AgeGroupKey string

const (
	ZeroThirty     AgeGroupKey = "0-30"
	ThirtyOneSixty AgeGroupKey = "31-60"
	SixtyUp        AgeGroupKey = "61+"
	NotApplicable  AgeGroupKey = "N/A"
)

type SummarizeData struct {
	Province map[string]uint
	AgeGroup map[AgeGroupKey]uint
}
