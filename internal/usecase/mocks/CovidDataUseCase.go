// Code generated by mockery v0.0.0-dev. DO NOT EDIT.

package mocks

import (
	entity "addzero/lineman/covid19/internal/entity"

	mock "github.com/stretchr/testify/mock"
)

// CovidDataUseCase is an autogenerated mock type for the CovidDataUseCase type
type CovidDataUseCase struct {
	mock.Mock
}

// GetSummary provides a mock function with given fields:
func (_m *CovidDataUseCase) GetSummary() (*entity.SummarizeData, error) {
	ret := _m.Called()

	var r0 *entity.SummarizeData
	if rf, ok := ret.Get(0).(func() *entity.SummarizeData); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*entity.SummarizeData)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
