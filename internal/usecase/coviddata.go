package usecase

import "addzero/lineman/covid19/internal/entity"

type CovidDataUseCase interface {
	GetSummary() (*entity.SummarizeData, error)
}
