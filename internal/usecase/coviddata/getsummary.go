package coviddata

import (
	"addzero/lineman/covid19/internal/entity"
	"fmt"
)

func summarize(data []entity.Data) (*entity.SummarizeData, error) {
	if data == nil {
		return nil, nil
	}

	sum := &entity.SummarizeData{
		Province: map[string]uint{},
		AgeGroup: map[entity.AgeGroupKey]uint{},
	}

	for _, d := range data {
		if d.ProvinceEn == nil {
			sum.Province["N/A"] += 1
		} else {
			sum.Province[*d.ProvinceEn] += 1
		}

		switch age := d.Age; {
		case age == nil:
			sum.AgeGroup[entity.NotApplicable] += 1
			break
		case *age < 31:
			sum.AgeGroup[entity.ZeroThirty] += 1
			break
		case *age < 61:
			sum.AgeGroup[entity.ThirtyOneSixty] += 1
			break
		case *age > 60:
			sum.AgeGroup[entity.SixtyUp] += 1
			break
		}
	}

	return sum, nil
}

func (c *covidDataUseCase) GetSummary() (*entity.SummarizeData, error) {
	raw, rawErr := c.dataRepository.GetRaw()
	if rawErr != nil {
		return nil, rawErr
	}

	if raw == nil {
		return nil, fmt.Errorf("raw data cannot be nil")
	}

	sum, sumErr := summarize(raw.Data)
	if sumErr != nil {
		return nil, sumErr
	}

	return sum, nil
}
