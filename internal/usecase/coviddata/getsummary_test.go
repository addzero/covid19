package coviddata

import (
	"addzero/lineman/covid19/internal/entity"
	"addzero/lineman/covid19/internal/repository/mocks"
	"addzero/lineman/covid19/shared/pointer"
	"fmt"
	"reflect"
	"testing"
)

func Test_covidDataUseCase_GetSummary(t *testing.T) {
	type mockers struct {
		dataRepoGetRawResult *entity.Response
		dataRepoGetRawError  error
	}
	tests := []struct {
		name    string
		mockers mockers
		want    *entity.SummarizeData
		wantErr bool
	}{
		{
			name: "should fails if get raw with error",
			mockers: mockers{
				dataRepoGetRawResult: &entity.Response{Data: []entity.Data{}},
				dataRepoGetRawError:  fmt.Errorf("get raw error"),
			},
			wantErr: true,
		},
		{
			name: "should fails if get raw result is nil",
			mockers: mockers{
				dataRepoGetRawResult: nil,
				dataRepoGetRawError:  nil,
			},
			wantErr: true,
		},
		{
			name: "should fails if get raw result is nil",
			mockers: mockers{
				dataRepoGetRawResult: nil,
				dataRepoGetRawError:  nil,
			},
			wantErr: true,
		},
		{
			name: "should success with expect summarize data if repo data get raw success",
			mockers: mockers{
				dataRepoGetRawResult: &entity.Response{Data: []entity.Data{
					{
						Age:        pointer.ToIntPtr(20),
						ProvinceEn: pointer.ToStringPtr("Samut Sakhon"),
					},
					{
						Age:        pointer.ToIntPtr(53),
						ProvinceEn: pointer.ToStringPtr("Samut Sakhon"),
					},
					{
						Age:        pointer.ToIntPtr(61),
						ProvinceEn: pointer.ToStringPtr("Samut Sakhon"),
					},
					{
						Age:        pointer.ToIntPtr(44),
						ProvinceEn: pointer.ToStringPtr("Bangkok"),
					},
					{
						Age:        nil,
						ProvinceEn: pointer.ToStringPtr("Bangkok"),
					},
				}},
				dataRepoGetRawError: nil,
			},
			want: &entity.SummarizeData{
				Province: map[string]uint{
					"Samut Sakhon": 3,
					"Bangkok":      2,
				},
				AgeGroup: map[entity.AgeGroupKey]uint{
					"0-30":  1,
					"31-60": 2,
					"61+":   1,
					"N/A":   1,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			dataRepo := new(mocks.CovidDataRepository)
			dataRepo.On("GetRaw").Return(tt.mockers.dataRepoGetRawResult, tt.mockers.dataRepoGetRawError)
			params := ParamCovidDataUseCase{
				DataRepository: dataRepo,
			}
			uc := NewCovidDataUseCase(params)
			got, err := uc.GetSummary()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetSummary() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetSummary() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_summarize(t *testing.T) {
	type args struct {
		data []entity.Data
	}
	tests := []struct {
		name    string
		args    args
		want    *entity.SummarizeData
		wantErr bool
	}{
		{
			name:    "should get nil for both data and error if data is nil",
			args:    args{data: nil},
			want:    nil,
			wantErr: false,
		},
		{
			name: "should get zero value summarize data if data is empty slice",
			args: args{data: []entity.Data{}},
			want: &entity.SummarizeData{
				Province: map[string]uint{},
				AgeGroup: map[entity.AgeGroupKey]uint{},
			},
			wantErr: false,
		},
		{
			name: "should get expect summarize data if data is exists 1",
			args: args{data: []entity.Data{
				{
					Age:        pointer.ToIntPtr(20),
					ProvinceEn: pointer.ToStringPtr("Samut Sakhon"),
				},
				{
					Age:        pointer.ToIntPtr(53),
					ProvinceEn: pointer.ToStringPtr("Samut Sakhon"),
				},
				{
					Age:        pointer.ToIntPtr(61),
					ProvinceEn: pointer.ToStringPtr("Samut Sakhon"),
				},
				{
					Age:        pointer.ToIntPtr(44),
					ProvinceEn: pointer.ToStringPtr("Bangkok"),
				},
				{
					Age:        nil,
					ProvinceEn: pointer.ToStringPtr("Bangkok"),
				},
			}},
			want: &entity.SummarizeData{
				Province: map[string]uint{
					"Samut Sakhon": 3,
					"Bangkok":      2,
				},
				AgeGroup: map[entity.AgeGroupKey]uint{
					"0-30":  1,
					"31-60": 2,
					"61+":   1,
					"N/A":   1,
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := summarize(tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("summarize() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("summarize() got = %v, want %v", got, tt.want)
			}
		})
	}
}
