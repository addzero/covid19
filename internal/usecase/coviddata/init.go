package coviddata

import (
	"addzero/lineman/covid19/internal/repository"
	"addzero/lineman/covid19/internal/usecase"
)

type covidDataUseCase struct {
	dataRepository repository.CovidDataRepository
}

type ParamCovidDataUseCase struct {
	DataRepository repository.CovidDataRepository
}

func NewCovidDataUseCase(param ParamCovidDataUseCase) usecase.CovidDataUseCase {
	return &covidDataUseCase{
		dataRepository: param.DataRepository,
	}
}
