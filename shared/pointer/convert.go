package pointer

func ToStringPtr(str string) *string {
	return &str
}

func ToUintPtr(u uint) *uint {
	return &u
}

func ToIntPtr(i int) *int {
	return &i
}
