package main

import (
	"addzero/lineman/covid19/internal/delivery/http/coviddata"
	coviddatarepo "addzero/lineman/covid19/internal/repository/coviddata"
	coviddatausecase "addzero/lineman/covid19/internal/usecase/coviddata"
	"context"
	"crypto/tls"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func setupHandler(ginEng *gin.Engine) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	httpClient := http.Client{Transport: tr}
	covidDataRepository := coviddatarepo.NewCovidDataRepository(
		coviddatarepo.ParamCovidDataRepository{ClientHttp: &httpClient})
	covidDataUseCase := coviddatausecase.NewCovidDataUseCase(
		coviddatausecase.ParamCovidDataUseCase{DataRepository: covidDataRepository})

	coviddata.NewCovidDataHandler(coviddata.ParamCovidDataHandler{
		GinEngine:        ginEng,
		CovidDataUseCase: covidDataUseCase,
	})
}

func main() {
	ginEng := gin.New()

	setupHandler(ginEng)

	srv := &http.Server{
		Addr: ":8000",
		Handler: ginEng,
	}
	
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("server start with error %v\n", err)
		}
	}()
	
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("server shutdown with error %v\n", err)
	}
	
	log.Println("server completely shutdown")
}
