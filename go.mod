module addzero/lineman/covid19

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/kr/pretty v0.1.0 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/stretchr/testify v1.7.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
